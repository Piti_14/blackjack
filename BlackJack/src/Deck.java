

public class Deck {
	public static final int SIZE = 52;
	private Card[] cards;
	private int numCards;
	
	public Deck(int numDecks) {
		numCards = numDecks*SIZE;
		cards = new Card[numDecks * SIZE];
		int counter = 0;
		Card c;
		for(int i = 0; i < numDecks; i++) {
			for(int j = 1; j <= 13; j++) {
				c = new Card(j, Suit.CLOVERS);
				cards[counter] = c;
				counter++;
				c = new Card(j, Suit.HEARTS);
				cards[counter] = c;
				counter++;
				c = new Card(j, Suit.DIAMONDS);
				cards[counter] = c;
				counter++;
				c = new Card(j, Suit.SPADES);
				cards[counter] = c;
				counter++;
			}
		}
		shuffleDeck();
	}
	
	public void shuffleDeck() {
		for(int i = 0; i < cards.length; i++) {
			int randomPos = (int)(Math.random() * cards.length);
			Card temp;
			temp = cards[i];
			cards[i] = cards[randomPos];
			cards[randomPos] = temp;
		}
	}
	
	public String toString() {
		String s = cards[0].toString();
		for(int i = 0; i < cards.length; i++) {
			s += ", " + cards[i];
		}
		return s;
	}
	
	public boolean isEmpty() {
		if(numCards == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public Card extractCard() {
		numCards--;
		return cards[numCards];
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
