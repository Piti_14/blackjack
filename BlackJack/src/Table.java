import java.util.Scanner;

public class Table {
	private Deck d;
	private Player[] players;
	private Dealer dealer;
	public static final int minBet = 5;
	private Scanner input;
	
	public Table(int numPlayers, int numDecks, Scanner input) {
		d = new Deck(numDecks);
		dealer = new Dealer();
		players = new Player[numPlayers];
		for(int i = 0; i < players.length; i++) {
			System.out.println("Player number " + (i+1));
			players[i] = Player.readFromKeyboard(input);
		}
	}
	
	public String toString() {
		String s = dealer.toString() + "\n";
		for(Player p : players) {
			s += p.toString() + "\n";
		}
		return s;
	}
	
	public boolean play() {
		boolean gameOver = false;
		while(!gameOver) {
			firstBets();
			firstDeal();
			boolean anyoneAlive = true;
			while(anyoneAlive) {
				restOfDeals();
				anyoneAlive = isAnyoneAlive();
			}
			playDealer();
			checkWinners();
			resetPlayers();
		}
		return true;
	}

	private void firstBets() {
		for(Player p : players) {
			System.out.println("Enter your bet: ");
			p.makeBet(input.nextInt());
		}
		
	}

	private void restOfDeals() {
		for(int i = 0; i < players.length; i++) {
			if(players[i].hasBlackJack()) {
				players[i].setCheck(true);
			} else {
				players[i].askCheck(input);
			}
		}
	}

	private void firstDeal() {
		for(int j = 0; j <= 1; j++) {
			for(int i = 0; i < players.length; i++) {
				players[i].dealCards(d.extractCard());
			}
			if(j == 0) {
				dealer.dealCards(d.extractCard());
			}
		}
	}

	private boolean isAnyoneAlive() {
		for(int i = 0; i < players.length; i++) {
			if(players[i].getCheck()) {
				return true;
			}
		}
		return false;
		
	}
	
	/*private void printWinners() {
		System.out.println("RESULTS: ---------");
		int valueCardsDealer = dealer.getCardsValue();
		if (dealer.isBusted()) {
			valueCardsDealer = -1;
		}		
		System.out.println(dealer);
		if (dealer.isBusted()) {
			System.out.println("--BUSTED--");
		}
		for (Player player: players) {
			System.err.println(player);
			if (!player.isBroke() && !player.isBusted()) {
				if (player.getCardsValue() > valueCardsDealer) {
					// Wins
					player.win();
					if (play)
				} else {
					if (player.getCardsValue() == valueCardsDealer) {
						// Even
						player.even();
					} else {
						System.out.println("-You lose-");
					}
				}
			}
		}
		
	}*/
}
