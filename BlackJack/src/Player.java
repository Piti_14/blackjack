import java.util.Scanner;

public class Player {
	String name;
	private int money;
	private Card[] cards;
	private int bet, pos;
	private boolean check;
	
	
	public Player(String name, int dollars) {
		this.name = name;
		money = dollars;
		cards = new Card[11];
		resetPlayer();
		
	}
	
	public static Player readFromKeyboard(Scanner input) {
		System.out.println("Enter name: \n");
		String name = input.next();
		System.out.println("Enter money: \n");
		int money = input.nextInt();
		Player p = new Player(name, money);
		
		return p;
	}
	
	public String toString() {
		String s = "Name: " + name + "\nMoney: " + money + "\n";
		for(int i = 0; i < getNumCards(); i++) {
			s += cards[getNumCards()] + " ";
		}
		return s;
	}
	
	public void makeBet(int bet) {
		if(bet > money) {
			bet = money;
		}else if(bet < Table.minBet) {
			this.bet = Table.minBet;
			money -= bet;
		} else {
			this.bet = bet;
			money -= bet;
		}
	}
	
	public boolean bankRupt() {
		if(money <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getBet() {
		return bet;
	}
	public int getMoney() {
		return money;
	}
	public boolean getCheck() {
		return check;
	}
	public void setCheck(boolean b) {
		check = b;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	
	public void dealCards(Card c) {
		while (cards[pos] != null) {
			pos++;
		}
		cards[pos] = c;
		pos++;
	}
	
	public int getScore() {
		pos = 0;
		int score = 0;
		while(cards[pos] != null) {
			score += cards[pos].getValue();
			pos++;
		}	
		return score;
	}
	
	public int getNumCards() {
		return pos;
	}
	
	public void win() {
		if(getNumCards() == 2 && getScore() == 21) {//blacki jacki
			money += bet * 2.5;
		} else {
			money += bet * 2;
		}
	}
	
	public void push() {
		money += bet;
	}
	
	public String cardsToString() {
		pos = 0;
		String s = "";
		while(cards[pos] != null) {
			s += cards[pos] + " ";
			pos++;
		}
		return s;
	}
	
	public void askCheck(Scanner input) {
		System.out.println("Do you want to check?(Y/N)");
		String answer = input.nextLine();
		if(answer.equalsIgnoreCase("Y")) {
			check = true;
		}
	}
	
	public void resetPlayer() {
		for(int i = 0; i < cards.length; i++) {
			cards[i] = null;
		}
		bet = 0;
		pos = 0;
		check = false;
	}

	public boolean hasBlackJack() {
		if(getNumCards() == 2 && getScore() == 21) {
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}