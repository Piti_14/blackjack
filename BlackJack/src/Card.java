
public class Card {
	private int number;
	private Suit suit;
	
	public Card(int n, Suit s) {
		number = n;
		suit = s;
	}
	
	@Override
	public String toString() {
		String s = "";
		switch(number) {
			case 1:
				s += "A";
				break;
			case 11:
				s += "J";
				break;
			case 12:
				s += "Q";
				break;
			case 13:
				s += "K";
				break;
			default:
				s += number;
		}
		switch(suit) {
			case CLOVERS:
				s += "♣";
				break;
			case SPADES:
				s += "♠";
				break;
			case HEARTS:
				s += "♥";
				break;
			case DIAMONDS:
				s += "♦";
				break;
		}
		return s;
	}
	
	public int getValue() {
		if(number <= 10) {
			return number;
		}else {
			return 10;
		}				
	}
}
